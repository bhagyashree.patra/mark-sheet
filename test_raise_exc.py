import pytest

from raise_exc import Student, StudentException as Except

@pytest.mark.parametrize("mark, result",
                        [
                            (20, "Fail"),
                            (40, "Pass")
                        ]
                        )
def test_check_result(mark, result):
    assert Student.check_result(mark) == result


@pytest.mark.parametrize("name, result",
                        [
                            ("John", "John"),
                            ("   Lucky ", "Lucky")
                            ])
def test_check_name(name, result):
    assert Student.check_name(name) == result

    with pytest.raises(Except, match="This name '' is not valid"):
        Student.check_name("")

    with pytest.raises(Except, match="This name '    ' is not valid"):
        Student.check_name("    ")

    with pytest.raises(Except, match="This name '123' is not valid"):
        Student.check_name("123")

    with pytest.raises(Except, match="This name ',' is not valid"):
        Student.check_name(",")

    with pytest.raises(Except, match="This name 'abcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabc' is not valid"):
        Student.check_name("abc" * 80)

def test_check_mark():
    assert Student.check_mark(20) == 20
    assert Student.check_mark(100) == 100
    assert Student.check_mark(0) == 0
    
    # assert Student.check_mark(-1) == "This mark -1 is not valid"
    with pytest.raises(Except, match="This mark -1 is not valid"):
        Student.check_mark(-1)

    # assert Student.check_mark(101) == "This mark 101 is not valid"
    with pytest.raises(Except, match="This mark 101 is not valid"):
        Student.check_mark(101)

# or: with pytest parameterize
# @pytest.mark.parametrize("mark, result",
#                         [
#                             (20, 20),
#                             (100, 100),
#                             (0, 0)
#                         ])
# def test_check_mark(mark, result):
#     assert Student.check_mark(mark) == result

@pytest.mark.parametrize("name, result",
                        [
                            ("John", True),
                            ("abc" * 80, False),
                            ("    ", False),
                            ("123", False),
                            (",", False),
                            ("", False)
                            ])
def test_name_validator(name, result):
    assert Student.name_validator(name) == result

# def test_name_validator():
#     assert Student.name_validator("John") is True
#     assert Student.name_validator("abc" * 80) is False
#     assert Student.name_validator("    ") is False
#     assert Student.name_validator("123") is False
#     assert Student.name_validator(",") is False
#     assert Student.name_validator("") is False

@pytest.mark.parametrize("mark, result",
                        [
                            (20, True),
                            (100, True),
                            (0, True),
                            (-1, False),
                            (101, False)
                        ])
def test_mark_validator(mark, result):
    assert Student.mark_validator(mark) == result

# def test_mark_validator():
#     assert Student.mark_validator(20) is True
#     assert Student.mark_validator(100) is True
#     assert Student.mark_validator(0) is True
#     assert Student.mark_validator(-1) is False
#     assert Student.mark_validator(101) is False

# pupil1 = Student("    ", 0) # fails
# pupil2 = Student("   Lucky ", 0) # should work but the name should be "Lucky"
# pupil3 = Student("123", 0) # should fail
# pupil4 = Student(",", 0) # should fail
# pupil5 = Student("", 0) # should fail
# pupil6 = Student("abc" * 80, 0) # should fail (names longer than 30 characters)
# pupil7 = Student("Lucky", -1) # should fail
# pupil8 = Student("Lucky", 101) # should fail
# pupil10 = Student("Lucky", "F") # should fail
