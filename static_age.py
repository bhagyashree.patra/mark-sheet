#!/usr/bin/env Python3

import time

class Category:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    @staticmethod
    def age_checker(age):
        if age <=50:
            return "young"
        else:
            return "old"
    
    @staticmethod
    def name_validator(name):
        if (name[0].islower()) or len(name) < 3 or (name.isdigit()) or (name.isspace()) or name.isupper():
            print("Invalid Name: %s" % name)
            time.sleep(2)
            print("Try again")
            time.sleep(2)
            name = input("Name: ")
            name_validator(name)
        else:
            return name
    
    @staticmethod
    def age_validator(age):
        if age < 0 or age > 150:
            print("Invalid Age: %s" % age)
            time.sleep(2)
            print("Try again")
            time.sleep(2)
            age = int(input("Age: "))
            age_validator(age)
        else:
            return age

    # @instancemethod # no need to put the decorator as by default it will take it as an instance method, if nothing else has been specified
    def display(self):
        self.name = input("Name: ")
        self.name_validator(self.name)
        self.age = int(input("Age: "))
        self.age_validator(self.age)
        result = self.age_checker(self.age)
        print(self.name, "is", result)

person = Category("", 0)

person.display()
