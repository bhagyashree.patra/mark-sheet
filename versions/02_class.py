#!/usr/bin/env python3

class Student:
    def check_pass_fail(self):
        if self.marks >= 70:
            return "Pass"
        else:
            return "Fail" 

pupil = Student()

def name_validation(name):
    if (name[0].islower()) or (name.isspace()) or (name.isdigit()) or len(name) < 3:
        print ("Please, enter a valid name")
        print("Try Again")
        name = input("Student Name: ")
        name_validation(name)
    else:
        return name

# pupil.name = input("Student Name: ")
# valid_name = name_validation(pupil.name)  
pupil.name = name_validation(input("Student Name: "))

pupil.marks = int(input("Student Marks: "))

result = pupil.check_pass_fail()
print (valid_name, "is",result)