#!/usr/bin/env python3

# Version_01: Interact with the user directly using "input", which doesn't recognizes the values("LUCKY" and -1) passed into it while calling from the class. # pupil = Student("LUCKY", -1) # pupil = Student("", 0)

import time

class Student:
    def __init__(self, name, mark):
        self.name = name
        self.mark = mark

    def check_result(self):
        if self.mark < 35:
            return "Fail"
        else:
            return "Pass"

    def name_validator(self):
        if (len(self.name) < 3) or self.name.isspace() or self.name[0].islower() or self.name.isdigit() or self.name.isupper():
            print("Invalid Name\nTry Again\n")
            self.name = input("Enter your name: ")
            self.name_validator()
        else:
            return self.name
    
    def mark_validator(self):
        if self.mark < 0 or self.mark > 100:
            print("Invalid Number\nTry Again\n")
            self.mark = int(input("Enter your mark: "))
            self.mark_validator()
        else:
            return self.mark

    def validation(self):
        self.name = input("Enter your name: ")
        self.name_validator()
        self.mark = int(input("Enter your mark: "))
        self.mark_validator()
        self.waiting()

    def display(self):
        self.validation()
        print("Name: ", self.name)
        print("Mark: ", self.mark)
        self.waiting()
        self.feedback()
        
    def waiting(self):
        print("......\n")
        time.sleep(2)

    def feedback(self):
        result = self.check_result()
        if result == "Pass":
            print("Congrats!! 🎉🎉 ",self.name, "has Passed the exam 🥳 \n ")
        else:
            print(self.name, "has failed the exam 😭😭😭!!\nBetter luck next time 👍 \n ")

# pupil = Student("", 0)
pupil = Student("LUCKY", -1)

pupil.display()