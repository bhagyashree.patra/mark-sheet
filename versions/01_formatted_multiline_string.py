#!/usr/bin/env python3
# class without self
class Student:
    pass 

student1 = Student()
student2 = Student()

student1.name = "John"
student1.age = 25
student1.marks = 90

student2.name = "Mary"
student2.age = 20
student2.marks = 80

# Practice of Formatted and Multiline Srings
print ('''Student 1
    \tName:{}
    \tAge:{}
    \tMarks:{}'''.format(student1.name, student1.age, student1.marks))

# Practice of %operator
print ('''Student 2
    \tName:%s
    \tAge:%d
    \tMarks:%d'''% (student2.name, student2.age, student2.marks))