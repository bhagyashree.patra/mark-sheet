#!/usr/bin/env python3

import time

class Student:
    # Constructor or init Method
    def __init__(self, name, mark):
        self.name = name
        self.mark = mark

    def validate_name(self):
        if self.name[0].islower() or self.name.isspace() or self.name.isdigit() or len(self.name) < 3 or self.name.isupper():
            print("Please, enter a valid name")
            time.sleep(1)
            print("Try Again")
            time.sleep(1)
            self.name = input("Student Name: ")
            self.name_validation()
        else:
            return self.name

    def validate_mark(self):
        if self.mark < 0 or self.mark > 100:
            print("Please, enter a valid mark")
            time.sleep(1)
            print("Try Again")
            time.sleep(1)
            self.mark = int(input("Student Marks: "))
            self.mark_validation()
        else:
            return self.mark         

    def check_pass_fail(self):
        if self.mark > 40:
            return "Pass"
        else:
            return "Fail"

    def display(self):
        print("Student Name:", self.name)
        self.name_validation()
        print("Student Marks:", self.mark)
        self.mark_validation()
        time.sleep(2)
        result = self.check_pass_fail()
        print (self.name, "is", result, ".")   

pupil = Student("Lucky", 0)
pupil.display()
