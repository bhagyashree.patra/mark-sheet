#!/usr/bin/env python3

import time

class Student:
    def __init__(self, name, mark):
        self.name = name
        self.mark = mark

    def check_result(self):
        if self.mark < 35:
            return "Fail"
        else:
            return "Pass"

    def name_validator(self):
        if (len(self.name) < 3) or self.name.isspace() or self.name[0].islower() or self.name.isdigit() or self.name.isupper():
            print("Invalid Name\nTry Again\n")
            self.name = input("Enter your name: ")
            self.name_validator()
        else:
            return self.name
    
    def mark_validator(self):
        if self.mark < 0 or self.mark > 100:
            print("Invalid Number\nTry Again\n")
            self.mark = int(input("Enter your mark: "))
            self.mark_validator()
        else:
            return self.mark

    def display(self):
        self.name = input("Enter your name: ")
        # print("Name: ", self.name)
        self.name_validator()
        self.mark = int(input("Enter your mark: "))
        # print("Mark: ", self.mark)
        self.mark_validator()
        print("......\n")
        time.sleep(2)
        self.feedback()

    def feedback(self):
        result = self.check_result()
        if result == "Pass":
            print("Congrats!! 🎉🎉 ",self.name, "has Passed the exam 🥳 \n ")
        else:
            print(self.name, "has failed the exam 😭😭😭!!\nBetter luck next time 👍 \n ")

# pupil = Student("", 0)

# pupil = Student() # name and mark will be asked interactively 
#pupil = Student("Lucky") # name will be assigned, mark will be asked interactively
pupil = Student("LUCKY", -1) # name and mark will be assigned

pupil.display()