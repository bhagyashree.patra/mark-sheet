#!/usr/bin/env python3

# pupil = Student() # name and mark will be asked interactively
# pupil = Student("Lucky") # name will be assigned, mark will be asked interactively
# pupil = Student("LUCKY", 0) # name and mark will be assigned

import time

class Student:
    # def __init__(self, name = "", mark = 0): // Here the problem is zero will be assigned to the marks by default
    def __init__(self, name = None, mark = None):
        self.name = name
        self.mark = mark

    def check_result(self):
        if self.mark < 35:
            return "Fail"
        else:
            return "Pass"

    def check_name(self):
        if self.name is None:
            self.name = input("Enter a valid name: ")
        else:
            return self.name

    def check_mark(self):
        if self.mark is None:
            self.mark = int(input("Mark Can't be 'None','Alphabet','Space' or 'Alphanumeric'\nEnter a valid mark: "))
        else:
            return self.mark

    def name_validator(self):
        self.check_name()
        if (len(self.name) < 3) or self.name.isspace() or self.name[0].islower() or self.name.isdigit() or self.name.isupper():
            print("Invalid Name\nTry Again\n")
            self.name = input("Enter your name: ")
            self.name_validator()
        else:
            return self.name
    
    def mark_validator(self):
        self.check_mark()
        if self.mark < 0 or self.mark > 100: 
            print("Invalid Number\nTry Again\n")
            self.mark = int(input("Enter your mark: "))
            self.mark_validator()
        else:
            return self.mark

    def validation(self):
        print("Name: ", self.name)
        self.name_validator()
        print("Mark: ", self.mark)
        self.mark_validator()
        self.waiting()

    def display(self):
        self.validation()
        print("Name: ", self.name)
        print("Mark: ", self.mark)
        self.waiting()
        self.feedback()

    def waiting(self):
        print("......\n")
        time.sleep(2)

    def feedback(self):
        result = self.check_result()
        if result == "Pass":
            print("Congrats!! 🎉🎉 ",self.name, "has Passed the exam 🥳 \n ")
        else:
            print(self.name, "has failed the exam 😭😭😭!!\nBetter luck next time 👍 \n ")

pupil = Student() # name will be assigned, mark will be asked interactively

pupil.display()