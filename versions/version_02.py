#!/usr/bin/env python3

# Version_01: Here the name and mark will be displayed which has been assigned while instantiating from the class; as we are using 'print'.
# For name and mark validation it will interact withe user as 'input' is used.
# pupil = Student("Lucky", -1) # name will be assigned, mark will be assigned

import time

class Student:
    def __init__(self, name, mark):
        self.name = name
        self.mark = mark

    def check_result(self):
        if self.mark < 35:
            return "Fail"
        else:
            return "Pass"

    def name_validator(self):
        if (len(self.name) < 3) or self.name.isspace() or self.name[0].islower() or self.name.isdigit() or self.name.isupper():
            print("\nInvalid Name\nTry Again\n")
            self.name = input("Enter your name: ")
            self.name_validator()
        else:
            return self.name
    
    def mark_validator(self):
        if self.mark < 0 or self.mark > 100:
            print("\nInvalid Number\nTry Again\n")
            self.mark = int(input("Enter your mark: "))
            self.mark_validator()
        else:
            return self.mark

    def validation(self):
        print("Name: ", self.name)
        self.name_validator()
        print("Mark: ", self.mark)
        self.mark_validator()
        self.waiting()

    def display(self):
        self.validation()
        print("Name: ", self.name)
        print("Mark: ", self.mark)
        self.waiting()
        self.feedback()
        
    def waiting(self):
        print("......\n")
        time.sleep(2)

    def feedback(self):
        result = self.check_result()
        if result == "Pass":
            print("Congrats!! 🎉🎉 ",self.name, "has Passed the exam 🥳 \n ")
        else:
            print(self.name, "has failed the exam 😭😭😭!!\nBetter luck next time 👍 \n ")

# pupil = Student("", 0)
pupil = Student("LUCKY", -1) # name and mark will be assigned

pupil.display()