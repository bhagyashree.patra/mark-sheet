#!/usr/bin/env python3

# Continuous loop of name and mark validation

import time
import sys

class Student:
    def __init__(self, name = None, mark = None):
        self.name = self.check_name(name)
        self.mark = self.check_mark(mark)

    def check_result(self):
        if self.mark < 35:
            return "Fail"
        else:
            return "Pass"

    def error(self, error_msg):
        print(error_msg, file = sys.stderr)
        sys.exit(1)

    def check_name(self, name):
        if name is None:
            name = input("Enter a valid name: ")
            return self.name_validator(name)
        else:
            return self.error("This name '" + name + "' is not valid")

    def check_mark(self, mark):
        if mark is None:
            mark = int(input("Mark Can't be 'None','Alphabet','Space' or 'Alphanumeric'\nEnter a valid mark: "))
            return self.mark_validator(mark)
        else:
            return self.error("This mark " + str(mark) + " is not valid")

    def name_validator(self, name):
        if (len(name) < 3) or name.isspace() or name[0].islower() or name.isdigit() or name.isupper():
            print("Invalid Name\nTry Again\n")
            name = input("Enter your name: ")
            return self.name_validator(name)
        else:
            return name
    
    def mark_validator(self, mark):
        if mark < 0 or mark > 100: 
            print("Invalid Number\nTry Again\n")
            mark = int(input("Enter your mark: "))
            return self.mark_validator(mark)
        else:
            return mark

    def display(self):
        self.waiting()
        print("Name: ", self.name)
        print("Mark: ", self.mark)
        self.waiting()
        self.feedback()

    def waiting(self):
        print("......\n")
        time.sleep(2)

    def feedback(self):
        result = self.check_result()
        if result == "Pass":
            print("Congrats!! 🎉🎉 ",self.name, "has Passed the exam 🥳 \n ")
        else:
            print(self.name, "has failed the exam 😭😭😭!!\nBetter luck next time 👍 \n ")

pupil = Student()

pupil.display()