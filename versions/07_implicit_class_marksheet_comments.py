#!/usr/bin/env python3

import sys

class Student:
    # Constructor or init Method
    def __init__(self, name = None, mark = None):
        self.name = self.check_name(name)
        self.mark = self.check_mark(mark)

    def error(self, error_message):
        print(error_message, file = sys.stderr)
        sys.exit(1)

    def check_name(self, name):
        if name is None:
            name = input("Enter a valid name: ")

        name_is_valid = self.validate_name(name)
        if (name_is_valid):
            return name
        else:
            self.error("The name '" + name + "' is not valid")

    def check_mark(self, mark):
        if mark is None:
            mark = input("Enter a valid mark: ")
        
        mark_is_valid = self.validate_mark(mark)
        if (mark_is_valid):
            return mark
        else:
            self.error("The mark " + mark + " is not valid")

        return mark

    def validate_name(self, name):
        "will return True is the name is valid, False otherwise"

        return False
        # if (name[0].islower()) or (name.isspace()) or (name.isdigit()) or len(name) < 3:
        #     print("Please, enter a valid name")
        #     print("Try Again")
        #     name = input("Student Name: ")
        #     self.name_validation()
        # else:
        #     return self.name

    def validate_mark(self, mark):
        "will return True is the mark is valid, False otherwise"

        return False
        # if self.mark < 0 or self.mark > 100:
        #     print("Please, enter a valid mark")
        #     print("Try Again")
        #     self.mark = int(input("Student Marks: "))
        #     self.mark_validation()
        # else:
        #     return self.mark         

    def check_pass_fail(self):
        if self.mark > 40:
            return "Pass"
        else:
            return "Fail"

    def display(self):
        print("Student Name:", self.name)
        # self.name_validation()
        print("Student Marks:", self.mark)
        # self.mark_validation()
        # result = self.check_pass_fail()
        # print (self.name, "is", result, ".")   

#pupil = Student() # name and mark will be asked interactively 
#pupil = Student("Lucky") # name will be assigned, mark will be asked interactively
#pupil = Student("Lucky", 0) # name and mark will be assigned

pupil1 = Student("    ", 0) # fails
pupil2 = Student("   Lucky ", 0) # should work but the name should be "Lucky"
pupil3 = Student("123", 0) # should fail
pupil4 = Student(",", 0) # should fail
pupil5 = Student("", 0) # should fail
pupil6 = Student("abc" * 80, 0) # should fail (names longer than 30 characters)
pupil7 = Student("Lucky", -1) # should fail
pupil8 = Student("Lucky", 101) # should fail
pupil10 = Student("Lucky", "F") # should fail


pupil1.display()
