#!/usr/bin/env python3

# Restrict the interactiveness by replacing with std error

import time
import sys

class Student:
    def __init__(self, name = None, mark = None):
        self.name = self.check_name(name)
        self.mark = self.check_mark(mark)

    def check_result(self):
        if self.mark < 35:
            return "Fail"
        else:
            return "Pass"

    def error(self, error_msg):
        print(error_msg, file = sys.stderr)
        sys.exit(1)

    def check_name(self, name):
        if name is None:
            name = input("Enter a valid name: ")

        valid_name = self.name_validator(name)

        if valid_name:
            return name
        else:
            return self.error("This name '" + name + "' is not valid")

    def check_mark(self, mark):
        if mark is None:
            mark = int(input("Mark Can't be 'None','Alphabet','Space' or 'Alphanumeric'\nEnter a valid mark: "))

        valid_mark = self.mark_validator(mark)

        if valid_mark:
            return mark
        else:
            return self.error("This mark " + str(mark) + " is not valid")

    def name_validator(self, name):
        if (len(name) < 3) or name.isspace() or name[0].islower() or name.isdigit() or name.isupper():
            return False
        else:
            return True
    
    def mark_validator(self, mark):
        if mark < 0 or mark > 100: 
            return False
        else:
            return True

    def display(self):
        self.waiting()
        print("Name: ", self.name)
        print("Mark: ", self.mark)
        self.waiting()
        self.feedback()

    def waiting(self):
        print("......\n")
        time.sleep(2)

    def feedback(self):
        result = self.check_result()
        if result == "Pass":
            print("Congrats!! 🎉🎉 ",self.name, "has Passed the exam 🥳 \n ")
        else:
            print(self.name, "has failed the exam 😭😭😭!!\nBetter luck next time 👍 \n ")

pupil = Student() # name will be assigned, mark will be asked interactively

pupil.display()