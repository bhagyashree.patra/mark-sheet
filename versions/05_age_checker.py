#!/usr/bin/env Python3

import time

class Category:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def age_checker(self):
        if self.age <=50:
            return "Young"
        else:
            return "Old"
    
    def name_validator(self):
        if (self.name[0].islower()) or len(self.name) < 3 or (self.name.isdigit()) or (self.name.isspace()) or self.name.isupper():
            print("Invalid Name: %s" % self.name)
            time.sleep(2)
            print("Try again")
            time.sleep(2)
            self.name = input("Name: ")
            self.name_validator()
        else:
            return self.name
    
    def age_validator(self):
        if self.age < 0 or self.age > 150:
            print("Invalid Age: %s" % self.age)
            time.sleep(2)
            print("Try again")
            time.sleep(2)
            self.age = int(input("Age: "))
            self.age_validator()
        else:
            return self.age

    def display(self):
        self.name = input("Name: ")
        self.name_validator()
        self.age = int(input("Age: "))
        self.age_validator()
        result = self.age_checker()
        print(self.name, "is", result)

person = Category("", 0)

person.display()
