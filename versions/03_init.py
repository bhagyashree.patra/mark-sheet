#!/usr/bin/env python3

class Student:
    # Constructor or init Method
    def __init__(self, name, mark):
        self.name = name
        self.mark = mark

    def display(self):
        print("Name: ", self.name)
        print("Mark: ", self.mark)  

    # def display(self, name, mark):
    #     print("Name: ", name)
    #     print("Mark: ", mark)

pupil = Student("Lucky", 10)
pupil.display()