#!/usr/bin/env python3

# Convert instance methods to static methods whereever necessary

import time
import sys

class StudentException(Exception):
    pass

class Student:
    def __init__(self, name = None, mark = None):
        self.name = Student.check_name(name)
        self.mark = Student.check_mark(mark)

    @staticmethod
    def check_result(mark):
        if mark < 35:
            return "Fail"
        else:
            return "Pass"

    @staticmethod
    def error(error_msg, raiseException = True):
        if (raiseException):
            raise StudentException(error_msg)
        else:
            print(error_msg, file = sys.stderr)
            sys.exit(1)
    
    @staticmethod
    def check_name(name):
        if name is None:
            name = input("Enter a valid name: ")

        is_valid_name = Student.name_validator(name)

        if is_valid_name:
            return name.strip()
        else:
            return Student.error("This name '" + name + "' is not valid")

    @staticmethod
    def check_mark(mark):
        if mark is None:
            mark = int(input("Mark Can't be 'None','Alphabet','Space' or 'Alphanumeric'\nEnter a valid mark: "))

        is_valid_mark = Student.mark_validator(mark)

        if is_valid_mark:
            return mark
        else:
            return Student.error("This mark " + str(mark) + " is not valid")

    @staticmethod
    def name_validator(name):
        if (len(name) < 3) or (len(name) > 30) or name.isspace() or name[0].islower() or name.isdigit() or name.isupper():
            return False
        else:
            return True
    
    @staticmethod
    def mark_validator(mark):
        if mark < 0 or mark > 100: 
            return False
        else:
            return True

    def display(self):
        Student.waiting()
        print("Name: ", self.name)
        print("Mark: ", self.mark)
        Student.waiting()
        self.feedback()

    @staticmethod
    def waiting():
        print("......\n")
        time.sleep(2)

    def feedback(self):
        result = Student.check_result(self.mark)
        if result == "Pass":
            print("Congrats!! 🎉🎉 ",self.name, "has Passed the exam 🥳 \n ")
        else:
            print(self.name, "has failed the exam 😭😭😭!!\nBetter luck next time 👍 \n ")

# pupil = Student() # name will be assigned, mark will be asked interactively

# pupil.display()
